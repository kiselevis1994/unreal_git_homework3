// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once 

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "HomeWork_3HUD.generated.h"

UCLASS()
class AHomeWork_3HUD : public AHUD
{
	GENERATED_BODY()

public:
	AHomeWork_3HUD();

	/** Primary draw call for the HUD */
	virtual void DrawHUD() override;

private:
	/** Crosshair asset pointer */
	class UTexture2D* CrosshairTex;

};

