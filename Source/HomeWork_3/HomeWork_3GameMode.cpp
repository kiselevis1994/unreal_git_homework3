// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "HomeWork_3GameMode.h"
#include "HomeWork_3HUD.h"
#include "HomeWork_3Character.h"
#include "UObject/ConstructorHelpers.h"

AHomeWork_3GameMode::AHomeWork_3GameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = AHomeWork_3HUD::StaticClass();
}
